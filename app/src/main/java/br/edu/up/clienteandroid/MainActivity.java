package br.edu.up.clienteandroid;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

  EditText txtEndereco;
  EditText txtMensagem;
  TextView txtRetorno;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_main);
    txtEndereco = (EditText) findViewById(R.id.txtEndereco);
    txtMensagem = (EditText) findViewById(R.id.txtMensagem);
    txtRetorno = (TextView) findViewById(R.id.txtRetorno);
  }

  public void onClickEnviar(View v) {

    String servidor = txtEndereco.getText().toString();
    String mensagem = txtMensagem.getText().toString();

    Processo processo = new Processo();
    processo.execute(servidor, mensagem);
  }

  public class Processo extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {

      String servidor = params[0];
      String mensagemDeEnvio = params[1];
      String mensagemDeRetorno = null;
      Socket socket = null;
      DataOutputStream dataOutputStream = null;
      DataInputStream dataInputStream = null;

      try {

        socket = new Socket(servidor, 8888);
        dataOutputStream = new DataOutputStream(socket.getOutputStream());
        dataInputStream = new DataInputStream(socket.getInputStream());
        dataOutputStream.writeUTF(mensagemDeEnvio);
        mensagemDeRetorno = dataInputStream.readUTF();

      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        if (socket != null) {
          try {
            socket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (dataOutputStream != null) {
          try {
            dataOutputStream.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (dataInputStream != null) {
          try {
            dataInputStream.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      return mensagemDeRetorno;
    }

    @Override
    protected void onPostExecute(String mensagemDeRetorno) {
      txtRetorno.setText(mensagemDeRetorno);
    }
  }
}
